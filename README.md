## 指派模块公共组件使用说明 ##

> AssignCommon.vue、AssignSearch.vue、less文件夹需默认要放在同一目录。也可将less目录提取到其他目录（如：assets），然后修改以上两个组件内的引入路径即可。

    import assignCommon from '路径/AssignCommon'

	<assign-common 
		v-if="assignShow"
        assign-title="关联课程分类/课程"
        assign-type="user"
		:user-show-tab="{user:true,group:true,position:true}"
        search-tip-text="搜索"
		:show-status.sync="assignShow"
        :show-data="assignShowData"
        :checked-keys="userKeys"
        :dept-checkbox="false"
		@ok="submitData">
    </assign-common>

## 参数说明（Attributes） ##
**assign-title(必填)**：指派窗口的标题

**assign-type(必填)**： 指派类型

> lesson：调用按课程分类指派

> user：按部门、用户组、职位

**dept-checkbox**：控制部门复选框是否可以勾选，仅在assign-type=user时生效

**user-show-tab**：当指派类型为user时，可通过参数设置控制tab展示。

**checked-keys**: 已选中项的uuid

**dept-checkbox**: 部门是否可选

> 例如：{user:true, group:true, position:true}

>**user**：按部门及用户指派 tab

>**group**：用户组 tab

>**position**：按职位 tab

**search-tip-text**：搜索框默认提示文字

**show-status(必填)**：子组件控制指派窗口是否展示

## 方法（Methods） ##
**ok(必填)**：指定接收选中数据的回调。submitData方法会返回子组件中选择的数据

    methods:{
		submitData (data){
			//data为选中的数据
		}
	}

## 数据格式（Data Format） ##

>返回数据中的 show_name（type:String） 字段，存储数据的名称



**按课程指派数据格式**

    {
		"lessonType":[
	        {
	            "uuid": "3B67C987-5863-4049-BB87-7CA842852BB3",
	            "name": "65e0810a690e52a87269",
				"checked":true  //默认选中状态
	        },
		],
		"lessonName":[
	        {
	            "uuid": "3B67C987-5863-4049-BB87-7CA842852BB3",
	             "title": "5fae8f6f",
				 "checked":false
	        },
		
		]
	}

**按用户及部门指派数据格式**

    {
		"deptList": [
	        {
	            "uuid": "22034553",
	            "dept_name": "部门1",
	            "show_name": "部门1"
	        }
		],
		"userList": [
	        {
	            "uuid": "0FE73D86-ECDF-4284-A1A3-DD41B14446F4",
				"user_name": "用户1",
				"show_name": "用户1"
	        }
    	]
	}

**按用户组及职位指派数据格式**

	{
	    "showList": [
	        {
	            "uuid": "22034553",
	            "name": "前端组",
	            "nameList": "薛崇伟，李璐，崔琳娜",
	        },
	        {
	            "uuid": "33366302",
	            "name": "后端组",
	            "nameList": "赵强，张绪文，强强",
	        },
	        {
	            "uuid": "666666",
	            "name": "UI设计",
	            "nameList": "崔美玲",
	        }
	    ]
	}