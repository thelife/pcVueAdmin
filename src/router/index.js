import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Ha from '@/components/ha'
import Respon from '@/components/responsive'
import Node from '@/components/node'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      children:[
        {
          path:'ha',
          name:'ha',
          component:Ha
        }
      ]
    },
    {
      path:'/res',
      name:'responsive',
      component:Respon
    },
    {
      path:'/node',
      name:'node',
      component:Node
    }
  ]
})
