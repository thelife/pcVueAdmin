const mockLesson = {
      "lessonType":[
        {
            "uuid": "3B67C987-5863-4049-BB87-7CA842852BB3",
            "name": "65e0810a690e52a87269",
           
        },
        {
            "uuid": "02FFD154-7AEC-4CFD-A5DE-C25D23A7EC5B",
            "name": "75af72c27684866b5b50",
           
        },
        {
            "uuid": "17ECCD6E-0AC0-4DCC-848E-0EB82E1FB056",
            "name": "5c0f718a732b",
           
        },
        {
            "uuid": "EE682B9B-71D9-4BBD-906B-AAC55352BF38",
            "name": "7b2c4e095b63",
          
        },
        {
            "uuid": "AE12A6D4-8100-47E7-AB31-438E9312EAF5",
            "name": "7985",
            
        }
      
    ],
    "lessonName": [
        {
            "uuid": "BFD6E9E7-185F-450C-B64F-3A8760EB39D1",
            "title": "5fae8f6f",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "D355843A-3A52-4572-ADC5-DE7F88A6903F",
            "title": "6d4b8bd563076d3e4fe1606f",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "A80B538A-F26D-4214-A45B-83CA0AD60EA3",
            "title": "6d4b8bd55fae514b65f6957f",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "DF584F65-73E6-4F2E-B1B5-C4031D953DB9",
            "title": "22222222222",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "F228DA2E-3238-4037-9B57-6DED0060E7B1",
            "title": "54c854c854c8",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "0B6001B5-782B-414A-BAD0-72FB4A8EB9C0",
            "title": "89c6989164ad653e6d4b8bd5",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "E3118720-FDFF-4C9B-8F5A-9F224DF0755A",
            "title": "67087167679d593476f8601d53f0",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "AF1A6D91-C271-498B-979F-29CB46B917FC",
            "title": "233054c854c82",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "8520CB1A-3CCC-41D8-A9B8-2DEF02B0F3DD",
            "title": "122954c854c854c854c854c8",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "F224C730-645F-4675-A5A0-9773239215C1",
            "title": "63888bfe6d4b8bd55fae514b",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "9DFED0F7-1AD9-4784-A136-BBD1096E72CF",
            "title": "76d163a7828270b96d4b8bd5",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "E7C420B6-CE39-4746-8BA1-FF45D4C0996B",
            "title": "76d163a7828270b96d4b8bd5",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "73AA49FF-C3B8-4590-BDC0-D1630596AABE",
            "title": "1229957f8bed97f35fae8bfe",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "EB2F08C1-F42A-46EF-AE33-361A8910BAA4",
            "title": "12285f5597f35fae8bfe",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "87473A6C-456D-40B5-A7A5-EC9553DEC53C",
            "title": "53d159276c34",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "9A5C8CF9-ED4D-49FA-8245-E757D3A87102",
            "title": "12275fae8bfe",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "3D5B4E91-FD7E-444A-9D32-829DE11787BE",
            "title": "1219968f580280038bd5",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "67CB4270-A122-46AF-A50D-F2B3997C1DE0",
            "title": "6d4b8bd563076d3e1",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "C2632CD6-A187-4B8C-8C41-5B2C6E9415DD",
            "title": "12313",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "F362C88E-888A-457B-A542-9EF5D8543B74",
            "title": "6d4b8bd5",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "A9E6B7DB-8436-4AC0-9D4C-DC1FE5019A6D",
            "title": "5fae8bfeh5538b7f295305",
            "name": "6d778d3c738b8def98de",
          
        },
        {
            "uuid": "E4D2D9A2-D4A3-41C2-8939-D71D21EE7377",
            "title": "6b637ecf76845fae8bfe",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "76BB7619-6CB6-4A6D-ACC2-AFCF49D732DC",
            "title": "biubiubiu2",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "331B7D45-A857-46FE-8121-7A689885A58C",
            "title": "biubiubiu",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "20323B35-27FE-44FB-B637-EF5A589DCED4",
            "title": "QQQ",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "43B4C40A-B054-48D9-A57E-CC5F117307B0",
            "title": "www",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "AF227461-2174-49D7-8307-64EC25C17756",
            "title": "8c0176849f3b5b50957f803367355927ff1f",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "6CFCF9EC-AFEC-4BBB-9FB8-3140358CF702",
            "title": "59278239",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "4A272A42-D5F9-474F-B78B-C587AC2C8B8D",
            "title": "6ed17fd4673a",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "9ADADDD7-D147-43C2-8E55-AA3DB9AC89C0",
            "title": "753552a881ea884c8f66",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "D7CE2516-10BE-4578-AFD5-5E2E7B500EF0",
            "title": "55105c9b6e7eB",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "6BB2215B-70BA-41E1-B9B4-42F633017443",
            "title": "55105c9b6e7eA",
            "name": "6d778d3c738b8def98de",
            
        },
        {
            "uuid": "9EABFB74-5C35-46EB-A494-68193940B11D",
            "title": "516d5e747ea7B",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "01B56124-9134-45B0-AE1F-1759EA1EAAEC",
            "title": "516d5e747ea7A",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "CAEC062E-B3D2-4517-AE0F-460A3CEE2625",
            "title": "9ec45c9b79d1628059275b66A8bfe7a0b",
            "name": "6d778d3c738b8def98de",
            
        },
        {
            "uuid": "4618968C-D06C-4D1F-ADA9-AF8A6887F5A7",
            "title": "4e2467619c7c",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "CFCF2EBA-68BF-4735-8A24-DE0D1369F3BF",
            "title": "4e0067619c7c",
            "name": "6d778d3c738b8def98de",
           
        },
        {
            "uuid": "BCC5C30C-7649-48F7-B967-CB40012B7C13",
            "title": "1206",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "57B01940-FE13-4691-9A83-19F7EA1DDA3F",
            "title": "950b522976846d4b8bd5",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "E1807ACB-1DDE-4E75-96B9-BB7D2D87ECC4",
            "title": "57238bde5feb4e50",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "458707E4-1BFA-4106-A9F5-804C60A3A82E",
            "title": "4f18901f72696d41",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "31FE4519-8BFF-49D0-A3F6-67F5565A6709",
            "title": "4f18901f72696d41",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "89148C41-34CE-4564-AAF4-675D38BB7309",
            "title": "4f18901f72696d41147",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "72504FF7-F5E5-49B4-BB0A-BAFA9FE758D8",
            "title": "4f18901f72696d41147",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "3480C98D-2C6B-4184-AA84-AABFB9C7A0F1",
            "title": "4f18901f72696d41",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "26E7E5A1-20EA-405B-9DD0-8591A2A786C1",
            "title": "4f18901f72696d41",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "16073376-4F06-4E7B-98A9-BC66E5FA0EBB",
            "title": "4f18901f72696d41",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "FEEA283E-945F-4F55-BCC3-015CD369B19E",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "73A40E47-D891-4491-BA0B-86EB0A31C62D",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "7B28465F-D0EF-4EFF-BFCF-445E53520145",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "513EC6E0-7E03-447B-BFA0-8FE04AA14729",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "3FB75F0F-CE4F-4114-915C-717240F8F3E3",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "49920BF6-E185-40BE-A566-BCCFE458A79A",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "554D0739-FB70-431C-875A-80093C323FA4",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "411B5A51-7213-4263-9D4F-2A62DF67C7F5",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "CA94A168-87B9-4B18-8625-B8045F13929E",
            "title": "63076d3e6d4b8bd51",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "12CBC32C-4071-4BAE-974C-E125F776B248",
            "title": "540354035403300230023002",
            "name": "6d778d3c738b8def98de",
        },
        {
            "uuid": "DCDD8C99-F5A1-4F65-87B1-BF9BC0872A41",
            "title": "(25d425e125d4)6d4b8bd5",
            "name": "6d778d3c738b8def98de",
        }
    ]
}
const mockUser = {
    "deptList": [
        {
            "uuid"         : "22034553",
            "dept_name"    : "4e007ea790e895e8",
            'has_children' : 0,
        },
        {
            "uuid": "33366302",
            "dept_name": "65b090e895e81",
            "disable" :true,
        },
        {
            "uuid": "53049245",
            "dept_name": "xinbumen",
        },
        {
            "uuid": "30179681",
            "dept_name": "dd90e895e8",
            "disable" :true
        },
        {
            "uuid": "33920717",
            "dept_name": "6d4b8bd590e895e8",
        },
        {
            "uuid": "37212527",
            "dept_name": "5e02573a90e8",
        },
        {
            "uuid": "37335108",
            "dept_name": "4ea754c190e8",
        },
        {
            "uuid": "37335116",
            "dept_name": "6280672f90e8",
        },
        {
            "uuid": "41072003",
            "dept_name": "4e1395e86d4b8bd5",
        },
        {
            "uuid": "47741180",
            "dept_name": "4e134e1a6d4b8bd5",
        }
    ],
    "userList": [
        {
            "dept_user_name": "4e0d591f",
            "user_name": "4e0d591f",
            "position": "",
            "uuid": "0FE73D86-ECDF-4284-A1A3-DD41B14446F4",
        }
    ]
}

const mockGroup = {
    "showList": [
        {
            "uuid": "22034553",
            "name": "前端组",
            "nameList": "薛崇伟，李璐，崔琳娜",
        },
        {
            "uuid": "33366302",
            "name": "后端组",
            "nameList": "赵强，张绪文，强强",
        },
        {
            "uuid": "666666",
            "name": "UI设计",
            "nameList": "崔美玲",
        }
    ]
}
export {mockLesson, mockUser, mockGroup}